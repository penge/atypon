<!--

  Senior frontend engineer at Atypon
  HTML5 prototype of an article page
  Pavel Bucka (bucka.pavel@gmail.com)

  Focusing on:
  1) Semantics and SEO
     HTML5 semantics like article, sections, aside, and figures.
     Meta tags:
     - social media tags,
     - standard meta tags,
     - robots,
     - app-capable.
  2) Vanilla JS
  3) Real data

  Animations:
  - using CSS transform translateX

  Page change:
  - by setting class on body [.page-main, .page-data]

  Responsiveness:
  - media queries
  - breakpoints 768, 900, 1024
  - body 2 column flex at 1024

  Navigation:
  - data-target [figures, references, related, details]
  - data-anim to trigger animation
-->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>3 great Washington's mountains you can't miss on your travels across US</title>

<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="description" content="3 great Washington's mountains on your travels across US" />
<meta name="keywords" content="Washington, mountains, travel, USA" />
<meta name="robots" content="index, follow" />

<link href="assets/styles/main.css" rel="stylesheet">
<link href="assets/styles/media.css" rel="stylesheet">

<meta property="og:title" content="3 great Washington's mountains you can't miss on your travels across US" />
<meta property="og:description" content="They're massive. Magical. Magnificent. Mountains inevitably put us in our place, and in Seattle, it's a place of unparalleled beauty and splendor." />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.atyponpress.com/travel/3-great-Washingtons-mountains-you-cant-miss-on-your-travels-across-US" />
<meta property="og:image" content="https://cdn.atyponpress.com/photos/02/28/18/578946/washington.jpg" />
<meta property="og:site_name" content="atyponpress.com" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@AtyponPress" />
</head>

<body class="page-main"><!--
- HEADER     (main Atypon header)
- MAIN       (holds article)
  - ARTICLE
    - HEADER
      - H1          (article's title)
      - AUTHORS     (unordered list)
      - NAVIGATION  (small navigation on mobile)
    - SECTIONS
      - #abstract
      - #introduction
      - #results

- HEADER            (header with the Back button)
- ASIDE             (place for all resources)
  - NAVIGATION      (big navigation)
  - SECTIONS
    - #figures      (contains figures with figcaptions)
    - #references
    - #related
    - #details -->


<!-- MAIN PAGE -->

<header class="header" id="atypon-header">
  <a href="#" id="logo">AtyponPress</a>
</header>

<main id="main">
  <article id="article">
    <header>
      <h1>3 great Washington's mountains you can't miss on your travels across US</h1>

      <ul id="authors">
        <li><a href="#">Roddy Scheer</a>,</li>
        <li><a href="#">Nick O’Connell</a>,</li>
        <li><a href="#">Tina Lassen</a></li>
      </ul>

      <ul class="navigation">
        <li data-anim="true" data-target="figures" class="active"><img src="assets/images/figures.svg" alt="figures" /></li>
        <li data-anim="true" data-target="references"><img src="assets/images/references.svg" alt="references" /></li>
        <li data-anim="true" data-target="related"><img src="assets/images/related.svg" alt="related" /></li>
        <li data-anim="true" data-target="details"><img src="assets/images/info.svg" alt="details" /></li>
      </ul>
    </header>

    <section>
      <h1>Abstract</h1>
      <p>We've all asked (and answered) the classic vacation question: The mountains or the shore? As it happens, we feel strongly both ways. But after publishing our <a href="#">Ultimate Coast Guide</a> three years ago, we knew we had created an obvious imperative. And so we present our paean to Washington's magnificent mountains.</p>
    </section>

    <section>
      <h1>Introduction</h1>
      <p>Why? Well, to paraphrase the famous quotation of British mountaineer <a href="#">George Mallory</a>, partly because they're there. But, more precisely, because they are ours. Here in Seattle, mountains inform our lives, whether we spend time hiking up them, skiing down them, or driving through them. (Some of us never set foot on them, and that's OK, too.)</p>
      <p>With brawny assertiveness, the <a href="#">Cascade Range</a> extends from northern California into southern British Columbia, a splendid serration of accessible beauty. In Washington, the range is both barrier — physical, climatological, even political — and unifier, bringing millions to its forested slopes every year in search of sumptuous scenery, sweet solitude and soft snow. West of Seattle, the <a href="#">Olympic Range</a> — whose glorious peaks never cease to impress us on a clear day — is a more distant tease, requiring a trip across (or around) Puget Sound to reach its remote retreats.</p>
      <p>Our lease agreement on all of these geological marvels comes with infinitely flexible terms — from an intimacy encouraged by frequent and fervent contact to an arm's-length relationship more comfortable with postcard views. Whatever the depth of the attraction, it is as steadfast as the mountains themselves.</p>
      <p>South of the summit, the winding road up to aptly named <a href="#">Paradise</a> (elevation: 5,400 feet) provides a fast track to the high and mighty impressive: neck-craning views of old-growth forests, alpine meadows and glaciers yawning with blue crevasses. Easy hiking trails start from the parking lot. Some weave among wildflowers and over streams. Others wander through glacial moraines and groves of alpine fir. And some climb steeply, serving as approaches for serious alpinists attempting the summit.</p>
      <p>The most accessible glaciated peak in North America, <a href="#">Rainier</a> became a national park in 1899, offering eye-popping displays of Northwest grandeur — ice-blue glaciers, multihued flowers, stunning waterfalls — for backseat tourists, hiking neophytes and hardy trekkers. The unprecedented access comes with a full helping of amenities: restaurants, a visitor center, gift shops and a classic wilderness lodge of peeled logs and massive stone fireplaces. With all these attractions, Rainier draws visitors from around the globe—between 1.5 million and 2 million a year. And most of them eventually turn up at Paradise.</p>
    </section>

    <section>
      <h1>Results</h1>
      <p>Which is why many who have made the trip once or twice actually prefer east-facing Sunrise to Paradise. The early-morning sun paints the snow-plastered northeast side of Rainier with an otherworldly light, wowing visitors and photographers who flock to Sunrise (elevation: 6,400 feet), the highest point in the park accessible by motor vehicle. This sublime spot, which is actually closer to Seattle, gives access to alpine meadows dotted with fir and carpeted in Indian paintbrush and avalanche lily. Our favorite part of this lesser-trod side of the mountain is the 360-degree view taking in <a href="#">Emmons Glacier</a>, the jagged line of Liberty Ridge — beloved by climbers — and distant summits of the Cascade Range gleaming on the horizon.</p>
    </section>
  </article>
</main>


<!-- OFFSCREEN PAGE -->

<header class="header" id="data-header">
  <a href="#" id="back">
    <img src="assets/images/back.svg" alt="Back" />
    <span>Back</span>
  </a>
</header>

<aside id="data">
  <ul class="navigation">
    <li data-target="figures" class="active">
      <img src="assets/images/figures.svg" alt="figures" />
      <span>Figures</span>
    </li>
    <li data-target="references">
      <img src="assets/images/references.svg" alt="references" />
      <span>References</span>
    </li>
    <li data-target="related">
      <img src="assets/images/related.svg" alt="related" />
      <span>Related</span>
    </li>
    <li data-target="details">
      <img src="assets/images/info.svg" alt="details" />
      <span>Details</span>
    </li>
  </ul>

  <section id="figures" class="active resource">
    <figure>
      <img src="assets/images/figure1.jpg" alt="Map">
      <figcaption><strong>Fig. 1.</strong> Climbing requires a hike-in of about eight miles, making any summit attempt at least a three-day affair.</figcaption>
    </figure>
    <figure>
      <img src="assets/images/figure2.png" alt="Teams">
      <figcaption><strong>Fig. 2.</strong> Climbers are usually friendly and open to adding new people to their group.</figcaption>
    </figure>
    <figure>
      <img src="assets/images/figure3.png" alt="Routes">
      <figcaption><strong>Fig. 3.</strong> Easy hiking trails start from the parking lot. Some weave among wildflowers and over streams.</figcaption>
    </figure>
  </section>

  <section id="references" class="resource listing">
    <section>
      <h1>Mount Olympus</h1>
      <ul>
        <li><a href="#">Navigator John Meares</a></li>
        <li><a href="#">Whistle tunes from the The Sound of Music in views of a vast</a></li>
        <li><a href="#">Olympic National Forest</a></li>
        <li><a href="#">Trails start at an elevation of 500 feet</a></li>
      </ul>
    </section>
    <section>
      <h1>Cascade Range</h1>
      <ul>
        <li><a href="#">Exploration of Puget Sound</a></li>
        <li><a href="#">Mount Rainier by Captain George</a></li>
        <li><a href="#">Pioneer settler Samuel M. Hamilton</a></li>
        <li><a href="#">British ambassador to Spain</a></li>
      </ul>
    </section>
  </section>

  <section id="related" class="resource listing">
    <section>
      <h1>Lodging</h1>
      <ul>
        <li><a href="#">The Essence of Health Retreat</a></li>
        <li><a href="#">Cowlitz tribe</a></li>
        <li><a href="#">B&B</a></li>
        <li><a href="#">Lavelatla</a></li>
        <li><a href="#">Mount Adams</a></li>
        <li><a href="#">Meadows Loop</a></li>
        <li><a href="#">Artist Point</a></li>
        <li><a href="#">Mount Baker Wilderness</a></li>
        <li><a href="#">National Recreation Area</a></li>
        <li><a href="#">Pacific Northwest</a></li>
        <li><a href="#">Ski Area</a></li>
        <li><a href="#">Wilderness</a></li>
      </ul>
    </section>
    <section>
      <h1>Elevation</h1>
      <ul>
        <li><a href="#">Seaquest State Park</a></li>
        <li><a href="#">State Route 504</a></li>
        <li><a href="#">Mount St. Helens: Alleyne Fitzherbert, first baron of St. Helens </a></li>
        <li><a href="#">Harmony Trail hike from the parking area at Harmony Viewpoint</a></li>
      </ul>
    </section>
    <section>
      <h1>Sighs</h1>
      <ul>
        <li><a href="#">Helens Helicopter Tours</a></li>
        <li><a href="#">Mount St. Helens</a></li>
        <li><a href="#">Spirit Lake Highway operated by the state Parks and Recreation Commission</a></li>
        <li><a href="#">25-minute tour</a></li>
        <li><a href="#">Terminating at Windy Ridge just four miles northeast of the crater</a></li>
        <li><a href="#">Weyerhaeuser Company</a></li>
        <li><a href="#">Johnston Ridge Observatory</a></li>
        <li><a href="#">Johnston Ridge from Castle Rock</a></li>
        <li><a href="#">Mount St. Helens National Volcanic Monument</a></li>
        <li><a href="#">Gifford Pinchot National Forest</a></li>
        <li><a href="#">Rocky Mountain Elk Foundation</a></li>
      </ul>
    </section>
  </section>

  <section id="details" class="resource listing">
    <section>
      <h1>Mountain Nomenclature</h1>
      <ul>
        <li><a href="#">Mount Adams: U.S. President John Adams</a></li>
        <li><a href="#">Mount Baker: Third Lieutenant Joseph Baker (a member of Vancouver's expedition)</a></li>
        <li><a href="#">Desolation Peak: So named after a forest fire swept the slopes bare in 1926</a></li>
        <li><a href="#">Glacier Peak: Based on an 1870 survey by Daniel Linsley</a></li>
        <li><a href="#">Hamilton Mountain: Pioneer settler Samuel M. Hamilton</a></li>
        <li><a href="#">Hurricane Ridge: For its gale-force winds</a></li>
        <li><a href="#">Mount Olympus: Named so by English navigator John Meares because it resembled a godlike paradise</a></li>
        <li><a href="#">Mount St. Helens: Alleyne Fitzherbert, first baron of St. Helens and British ambassador to Spain (another friend of Vancouver's)</a></li>
      </ul>
    </section>
    <section>
      <h1>Snow Cone: Mount Baker</h1>
      <ul>
        <li><a href="#">Among the names accorded Mount Baker by indigenous people</a></li>
        <li><a href="#">Northernmost of the Cascade volcanoes in the U.S.</a></li>
        <li><a href="#">Mount Baker was made on August 17, 1868, by Edmund T. Coleman</a></li>
        <li><a href="#">Koma Kulshan ("Great White Watcher")</a></li>
        <li><a href="#">Lower 48 — after Mount Rainier</a></li>
        <li><a href="#">Mount Baker Ski Area in the winter of 1998-99</a></li>
        <li><a href="#">95 feet of snow — 1,140 inches</a></li>
        <li><a href="#"> The massive snowpack permits safe glacier skiing well into summer</a></li>
      </ul>
    </section>
  </section>
</aside>

<script type="text/javascript" src="assets/scripts/bundle.js"></script>
</body>

</html>