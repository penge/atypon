/*
  BUNDLE.JS

  Change Menu and Resource by:
  - click on .navigation li
  - LEFT and RIGHT arrow keys
    - going all way LEFT jumps to the end
    - going all way RIGHT jumps to the beginning

  Resource is identified by target:
  - [figures, references, related, details]

  Reset scrollTop:
  - when opening offscreen page
  - when going back to main page
  - when changing tabs

  Update Active Menu and Resource
  - only one Resource active
  - only one Menu item active (in both menus)
*/

(function() {
  "use strict";

  var targets       = ["figures", "references", "related", "details"];
  var currentTarget = "figures";
  var navitems      = Array.from(document.querySelectorAll(".navigation li"));
  var resources     = Array.from(document.getElementsByClassName("resource"));


  // Helpers
  var removeActive = function(node) { node.classList.remove("active") };
  var addActive    = function(node) { node.classList.add("active") };


  // Events
  back.addEventListener("click", close);
  navitems.forEach(function(node) { node.addEventListener("click", open) });
  document.addEventListener("keyup", function(e) {
    var charCode = e.keyCode || e.which;
    if (charCode != 37 && charCode != 39) { return; }
    var index = targets.indexOf(currentTarget);
    index = (charCode == 37) ? index - 1 : index + 1;
    if (index == -1) { index = targets.length - 1; }
    else if (index == targets.length) { index = 0; }
    setActive(targets[index]);
  });


  // Set Active Menu and Resource
  function setActive(target) {
    currentTarget = target;

    // Remove Active
    navitems.forEach(removeActive);
    resources.forEach(removeActive);

    // Add Active
    var index = targets.indexOf(currentTarget);
    addActive(navitems[index]);
    addActive(navitems[index + targets.length]);
    addActive(resources[index]);
  }


  function scrollTop(node) { node.scrollTop = 0; }
  function setPage(page)   { document.body.className = page; }
  function close()  { scrollTop(main); setPage("page-main"); }
  function open() {
    setActive(this.dataset.target);
    if (this.dataset.anim) { scrollTop(data); setPage("page-data"); }
    else { scrollTop(resources[targets.indexOf(currentTarget)]); }
  }
})();